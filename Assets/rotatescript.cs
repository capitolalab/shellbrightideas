﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotatescript : MonoBehaviour
{
    public float speed;
    float rotation;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        rotation += speed * Time.deltaTime;
        transform.Rotate(Vector3.up, speed, Space.World);
    }
}
