﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Singleton<AudioManager>
{
    AudioSource[] sideSources;
    [System.Serializable]
    public struct SideStruct
    {
        public AudioClip openClip, closeClip;
        [Range(0, 1)] public float openDelay, closeDelay;
    }
    [SerializeField] SideStruct HologramSide, PrinterSide, VaultSide, TelescopeSide, SpringSide, DoorSide;

    private Dictionary<Side.SideAnimation, SideStruct> _cubeSFX = new Dictionary<Side.SideAnimation, SideStruct>();

    // Start is called before the first frame update
    void Start()
    {
        _cubeSFX[Side.SideAnimation.HOLOGRAM] = HologramSide;
        _cubeSFX[Side.SideAnimation.PRINTER] = PrinterSide;
        _cubeSFX[Side.SideAnimation.VAULT] = VaultSide;
        _cubeSFX[Side.SideAnimation.TELESCOPE] = TelescopeSide;
        _cubeSFX[Side.SideAnimation.SPRING] = SpringSide;
        _cubeSFX[Side.SideAnimation.DOORS] = DoorSide;

        sideSources = GetComponents<AudioSource>();
    }

    public void StartNewSound(Side.SideAnimation sideAnimation, bool isStarting)
    {
        if (sideSources[(int)sideAnimation].isPlaying)
        {
            sideSources[(int)sideAnimation].Stop();
        }
        if (isStarting)
        {
            sideSources[(int)sideAnimation].clip = _cubeSFX[sideAnimation].openClip;
            sideSources[(int)sideAnimation].PlayDelayed(_cubeSFX[sideAnimation].openDelay);
        }
        else
        {
            sideSources[(int)sideAnimation].clip = _cubeSFX[sideAnimation].closeClip;
            sideSources[(int)sideAnimation].PlayDelayed(_cubeSFX[sideAnimation].closeDelay);
        }
    }
}
