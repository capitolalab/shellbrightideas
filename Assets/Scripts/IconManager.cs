using DG.Tweening;
using UnityEngine;

public class IconManager : MonoBehaviour
{
    Vector3 _iconShownScale, _iconShownPosition;

    Sequence _inVisionSequence, _outOfVisionSequence;

    public static IconManager Instance = null;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        _iconShownScale = new Vector3(30, 30, 30);
        _iconShownPosition = new Vector3(0, 100, 0);
    }

    public void ShowIconAnimation(TweenCallback onPlayCallback, Transform icon, float animationTime)
    {
        if (_outOfVisionSequence != null)
        {
            _outOfVisionSequence.Kill();
        }
        _inVisionSequence = DOTween.Sequence();
        _inVisionSequence.OnPlay(onPlayCallback)
                        .Append(icon.DOScale(_iconShownScale, animationTime).SetEase(Ease.InOutQuad))
                        .Join(icon.DOLocalMove(_iconShownPosition, animationTime).SetEase(Ease.InOutQuad))
                        .Play();
    }

    public void HideIconAnimation(TweenCallback onCompleteCallback, Transform icon, float animationTime)
    {
        if (_inVisionSequence != null)
        {
            _inVisionSequence.Kill();
        }
        _outOfVisionSequence = DOTween.Sequence();
        _outOfVisionSequence.Append(icon.DOScale(Vector3.zero, animationTime).SetEase(Ease.InOutQuad))
                            .Join(icon.DOLocalMove(Vector3.zero, animationTime).SetEase(Ease.InOutQuad))
                            .OnComplete(onCompleteCallback)
                            .Play();
    }
}