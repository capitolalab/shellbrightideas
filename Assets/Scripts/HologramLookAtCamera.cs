﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class HologramLookAtCamera : MonoBehaviour
{
    VideoPlayer _videoPlayer;
    Camera _mainCamera;
    [SerializeField] Transform _multiTarget;

    // Start is called before the first frame update
    void Start()
    {
        _videoPlayer = GetComponent<VideoPlayer>();
        _mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        LookTowardsCamera();
    }

    private void LookTowardsCamera()
    {
        Vector3 dir = (this.transform.position - _mainCamera.transform.position).normalized;
        // dir.y = 0;

        Vector3 cubeRotation = _multiTarget.transform.right;

        cubeRotation.x = Mathf.Abs(cubeRotation.x);
        cubeRotation.y = Mathf.Abs(cubeRotation.y);
        cubeRotation.z = Mathf.Abs(cubeRotation.z);

        cubeRotation = Vector3.one - cubeRotation;

        dir.x *= cubeRotation.x;
        dir.y *= cubeRotation.y;
        dir.z *= cubeRotation.z;

        float value;

        if (_multiTarget.rotation.eulerAngles.y > 45 && _multiTarget.rotation.eulerAngles.y <= 135)
        {
            value = -_multiTarget.rotation.eulerAngles.x;
        }
        else if (_multiTarget.rotation.eulerAngles.y > 135 && _multiTarget.rotation.eulerAngles.y <= 225)
        {
            value = -_multiTarget.rotation.eulerAngles.z - 90;
        }
        else if (_multiTarget.rotation.eulerAngles.y > 225 && _multiTarget.rotation.eulerAngles.y <= 315)
        {
            value = _multiTarget.rotation.eulerAngles.x;
        }
        else
        {
            value = _multiTarget.rotation.eulerAngles.z + 90;
        }

        transform.rotation = Quaternion.LookRotation(dir) * Quaternion.Euler(0, 0, value);
    }
}