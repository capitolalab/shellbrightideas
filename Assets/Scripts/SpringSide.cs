﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using DG.Tweening;
public class SpringSide : BaseSide
{
    //[SerializeField] PlayVideos playVideos;
    bool isClosing = false;
    [SerializeField] MeshRenderer[] springMeshes;
    [SerializeField] AnimationClip openClip, closeClip;

    private void Start()
    {
        CustomStart();
    }

    protected override void CustomStart()
    {
        base.CustomStart();
        HideSpring();
    }

    override public void OnGaze()
    {
        base.OnGaze();
        OpenSpring();
    }

    override public void OutOfGaze()
    {
        base.OutOfGaze();
        CloseSpring();
    }

    private void Update()
    {
        CustomUpdate();
    }

    override protected void CustomUpdate()
    {
        base.CustomUpdate();
        if (isClosing && !GetComponent<Animation>().isPlaying)
        {
            HideSpring();
            isClosing = true;
        }
    }

    //Open doors when plane is (close to) facing camera
    void OpenSpring()
    {
        //HideIcon();
        openAnimation[openAnimation.clip.name].speed = 1;
        openAnimation.clip = openClip;
        openAnimation.Play();
        ShowSpring();
        isClosing = false;
        //playVideos.SetupVideoPlane((int)sideLocation, Transform);
    }

    //Close doors when video is done playing or plane is no longer facing camera
    void CloseSpring()
    {
        openAnimation[openAnimation.clip.name].speed = 1;
        openAnimation.clip = closeClip;
        openAnimation.Play();
        isClosing = true;
    }

    void HideSpring()
    {
        foreach (MeshRenderer springMesh in springMeshes)
        {
            springMesh.enabled = false;
        }
    }

    void ShowSpring()
    {
        foreach (MeshRenderer springMesh in springMeshes)
        {
            springMesh.enabled = true;
        }
    }

    public override void ResetAnimation()
    {
        base.ResetAnimation();
        openAnimation.clip = closeClip;
        openAnimation[openAnimation.clip.name].time = openAnimation[openAnimation.clip.name].length;
        openAnimation[openAnimation.clip.name].speed = 1;
        openAnimation?.Play();
        base.StopVideo();
    }
}
