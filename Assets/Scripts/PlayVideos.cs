﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using DG.Tweening;

public class PlayVideos : MonoBehaviour
{
    [NamedArrayAttribute(new string[] { "Top Video", "Bottom Video", "Left Video", "Right Video", "Front Video", "Back Video" }), SerializeField]
    VideoClip[] videoClips;

    [SerializeField]
    VideoPlayer videoPlayer;
    float scaleTime = 0.5f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Vector3 rotation = transform.localRotation.eulerAngles;
        //rotation.x = 0;
        //transform.rotation = Quaternion.Euler(rotation);
    }

    //Set correct video for activated side
    public void SetupVideoPlane(int side, Transform planeTransform)
    {
        videoPlayer.clip = videoClips[side];
        Vector3 rotation = planeTransform.localRotation.eulerAngles;
        rotation.x += 90;
        transform.localRotation = Quaternion.Euler(rotation);
        videoPlayer.Play();
        transform.DOScale(new Vector3(0.1f, 0.1f, 0.1f), scaleTime).SetEase(Ease.InOutQuad);
    }

    public void StopVideo()
    {
        videoPlayer.Stop();
        transform.DOScale(Vector3.zero, 0.2f).OnComplete(videoPlayer.Play).SetEase(Ease.InOutQuad);
    }
    //Rotate plane, scale plane and load video at the same time
    //Play video when rotating and scaling is done
    //Pause video on exit


}
