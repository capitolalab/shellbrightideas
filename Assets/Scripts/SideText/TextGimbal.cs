﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextGimbal : MonoBehaviour
{   
    [SerializeField]
    Transform videoCubeTransform;

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(Camera.main.transform);
        transform.position = videoCubeTransform.position;
    }
}
