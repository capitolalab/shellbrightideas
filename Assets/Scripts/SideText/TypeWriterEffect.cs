﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TypeWriterEffect : Singleton<TypeWriterEffect>
{
    [SerializeField]
    float delay;
    [SerializeField]
    string topText, bottomText, leftText, rightText, backText, frontText;
    //[StringArrayAttribute(new string[] { "Top Text", "Bottom Text", "Left Text", "Right Text", "Back Text", "Front Text" }), SerializeField]
    string[] sideTextContent;
    WaitForSeconds waitForDelay;
    TextMeshPro textDisplay;
    Coroutine textPrint;


    // Start is called before the first frame update
    void Start()
    {
        sideTextContent = new string[] {topText, bottomText, leftText, rightText,  backText, frontText};
        waitForDelay = new WaitForSeconds(delay);
        textDisplay = GetComponent<TextMeshPro>();
    }

    public void TypeNewString(int sideIndex)
    {
       if(textPrint != null)
            StopCoroutine(textPrint);
        textPrint = StartCoroutine(TypeText(sideTextContent[sideIndex]));
    }

    public void ClearString()
    {
        textDisplay.text = "";
    }

    IEnumerator TypeText(string textToBeDisplayed)
    {
        for (int i = 0; i < textToBeDisplayed.Length + 1; i++)
        {
            textDisplay.text = textToBeDisplayed.Substring(0, i);
            yield return waitForDelay;
        }
    }
}
