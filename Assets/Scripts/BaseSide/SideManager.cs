﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SideManager : MonoBehaviour
{
    private GameObject _mainCamera;
    private bool _isTracking;
    private List<Transform> children;

    [SerializeField]
    private List<GameObject> _sideGameObjects;
    private List<ISide> _sides = new List<ISide>();
    private Dictionary<Side.SideLocation, bool> _activeSides = new Dictionary<Side.SideLocation, bool>();

    [SerializeField]
    private Renderer[] showHideRenderers;
    [SerializeField]
    private Renderer[] hideOnlyRenderers;

    //private Side.SideLocation _hologramSide = Side.SideLocation.TOP;
    private Side.SideLocation _printerSide = Side.SideLocation.FRONT;

    void Awake()
    {
        _mainCamera = Camera.main.gameObject;

        SetActiveSides(true);
        LoadSideObjects();
        SetActiveOnChildren(false);
    }

    void Update()
    {
        if (_isTracking)
        {
            //LogArray(_activeSides);
            foreach (ISide side in _sides)
            {
                UpdateSide(side);
            }
        }
    }

    private void LogArray(Dictionary<Side.SideLocation, bool> activeSides)
    {
        string logMessage = string.Empty;
        foreach (Side.SideLocation side in activeSides.Keys)
        {
            logMessage += $" | {side} = {activeSides[side]}";
        }
        Debug.Log(logMessage);
    }

    public void UpdateSide(ISide side)
    {
        if (SideIsInVision(side))
        {
            if (!SideIsActive(side)) return;

            if (SideIsInGaze(side))
            {
                EnableGaze(side);
            }
            else
            {
                DisableGaze(side);
            }
        }
        else
        {
            if (side.IsInGaze)
            {
                side.OutOfGaze(); // So the top side gets properly reset when looking at the bottom side
                SetActiveSides(true);
            }
            side.OutOfVision();
        }
    }

    private void SetActiveOnChildren(bool value)
    {
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(value);
        }
    }

    private void LoadSideObjects()
    {
        foreach (GameObject side in _sideGameObjects)
        {
            _sides.Add(side.transform.GetComponent<ISide>());
        }
    }

    private void SetActiveSides(bool value)
    {
        foreach (Side.SideLocation location in (Side.SideLocation[])Enum.GetValues(typeof(Side.SideLocation)))
        {
            _activeSides[location] = value;
        }
    }

    private bool SideIsActive(ISide side)
    {
        return _activeSides[side.GetSideLocation];
    }

    private bool SideIsInVision(ISide side)
    {
        return Vector3.Dot(_mainCamera.transform.forward, side.Transform.up) <= 0.2f;
    }

    private bool SideIsInGaze(ISide side)
    {
        return Vector3.Dot(_mainCamera.transform.forward, side.Transform.up) <= -0.8f;
    }

    private void EnableGaze(ISide side)
    {
        if (side.IsInGaze) return;

        SetActiveSides(true);

        if (side.IsInVision)
        {
            side.OutOfVision();
        }

        side.OnGaze();
        //if (side.GetSideLocation == _hologramSide)
        //{
        //    // This if statement is used to tackle the edge case where you would look to the top after looking at the front
        //    if (_sides[(int)_printerSide].IsInGaze)
        //    {
        //        DisableGaze(_sides[(int)_printerSide]);
        //    }
        //    SetActiveSides(false);
        //    _activeSides[Side.SideLocation.BOTTOM] = true;
        //}
        if (side.GetSideLocation == _printerSide)
        {
            _activeSides[_printerSide] = false;
            _activeSides[Side.SideLocation.BOTTOM] = false;
        }
    }

    private void DisableGaze(ISide side)
    {
        if (side.IsInGaze)
        {
            side.OutOfGaze();
            if (side.GetSideLocation == _printerSide)
            {
                SetActiveSides(true);
            }
        }
        if (!side.IsInVision)
        {
            side.InVision();
        }
    }

    public void TrackingFound()
    {
        SetActiveOnChildren(true);
        SetRenderersActive(showHideRenderers, true);
        _isTracking = true;
    }

    private void SetRenderersActive(Renderer[] renderers, bool value)
    {
        foreach (Renderer renderer in renderers)
        {
            renderer.enabled = value;
        }
    }

    public void TrackingLost()
    {
        if (_isTracking)
        {
            foreach (GameObject side in _sideGameObjects)
            {
                side.GetComponent<BaseSide>().ResetAnimation();
            }
            VideoManager.Instance.StopPlayingAllVideos();
            _isTracking = false;
            foreach (ISide side in _sides)
            {
                side.ResetAnimation();
                if (side.IsInGaze)
                {
                    side.IsInGaze = false;
                }
                if (side.IsInVision)
                {
                    side.IsInVision = false;
                }
            }
            SetRenderersActive(showHideRenderers, false);
            SetRenderersActive(hideOnlyRenderers, false);
            SetActiveSides(true);
        }
    }
}
