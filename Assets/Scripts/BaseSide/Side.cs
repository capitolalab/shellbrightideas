using UnityEngine;

public abstract class Side : MonoBehaviour
{
    private bool isInVision, isInGaze;
    public enum SideLocation { TOP, BOTTOM, LEFT, RIGHT, BACK, FRONT }
    [SerializeField] private SideLocation sideLocation;
    public enum SideAnimation { HOLOGRAM, PRINTER, VAULT, TELESCOPE, SPRING, DOORS }
    [SerializeField] private SideAnimation sideAnimation;

    public Transform Transform
    {
        get { return this.transform; }
    }

    public bool IsInVision
    {
        get { return isInVision; }
        set { isInVision = value; }
    }

    public bool IsInGaze
    {
        get { return isInGaze; }
        set { isInGaze = value; }
    }

    public SideLocation GetSideLocation
    {
        get { return sideLocation; }
    }

    public SideAnimation GetSideAnimation
    {
        get { return sideAnimation; }
    }
}