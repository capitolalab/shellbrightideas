using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISide
{
    Transform Transform { get; }
    bool IsInVision { get; set; }
    bool IsInGaze { get; set; }
    Side.SideLocation GetSideLocation { get; }
    Side.SideAnimation GetSideAnimation { get; }
    void InVision();
    void OutOfVision();
    void OnGaze();
    void OutOfGaze();
    void ResetAnimation();
}