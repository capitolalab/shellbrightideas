using DG.Tweening;
using UnityEngine;
using UnityEngine.Video;

public class BaseSide : Side, ISide
{
    [SerializeField] protected float openTime;
    [SerializeField] protected Transform _iconTransform;
    [SerializeField] protected Animation openAnimation;

    protected VideoPlayer video;

    private Transform _mainCameraTransform;
    protected bool _iconVisible;
    private float _showIconThreshold = 0.5f;

    private void Start()
    {
        CustomStart();
    }

    protected virtual void CustomStart()
    {
        _mainCameraTransform = Camera.main.transform;
    }

    private void Update()
    {
        CustomUpdate();
    }

    protected virtual void CustomUpdate()
    {
        IconRotateToCamera();

        if (this.IsInVision)
        {
            if (_iconVisible)
            {
                if (Vector3.Dot(transform.up, Vector3.right) > _showIconThreshold)
                {
                    // IconManager.Instance.HideIconAnimation(HideIcon, _iconTransform, openTime / 3);
                }
            }
            else
            {
                if (Vector3.Dot(transform.up, Vector3.right) < _showIconThreshold)
                {
                    // IconManager.Instance.ShowIconAnimation(ShowIcon, _iconTransform, openTime / 3);
                }
            }
        }
    }

    protected void IconRotateToCamera()
    {
        Vector3 dir = (_iconTransform.position - _mainCameraTransform.position).normalized;
        _iconTransform.rotation = Quaternion.LookRotation(dir);
    }

    public virtual void InVision()
    {
        IsInVision = true;
        // IconManager.Instance.ShowIconAnimation(ShowIcon, _iconTransform, openTime / 3);
    }

    public virtual void OnGaze()
    {
        IsInGaze = true;
        // IconManager.Instance.HideIconAnimation(HideIcon, _iconTransform, openTime / 3);
        TypeWriterEffect.Instance.TypeNewString((int)GetSideLocation);
        AudioManager.Instance.StartNewSound(GetSideAnimation, IsInGaze);

    }

    public virtual void OutOfGaze()
    {
        IsInGaze = false;
        // IconManager.Instance.ShowIconAnimation(ShowIcon, _iconTransform, openTime / 3);
        TypeWriterEffect.Instance.ClearString();
        AudioManager.Instance.StartNewSound(GetSideAnimation, IsInGaze);
    }

    public virtual void OutOfVision()
    {
        IsInVision = false;
        // IconManager.Instance.HideIconAnimation(HideIcon, _iconTransform, openTime / 3);
    }

    protected void ShowIcon()
    {
        _iconTransform.gameObject.SetActive(true);
        _iconVisible = true;
    }

    protected void HideIcon()
    {
        _iconTransform.gameObject.SetActive(false);
        _iconVisible = false;
    }

    void StartVideo()
    {
        VideoManager.Instance.PlayVideo((int)GetSideLocation);
    }

    public void StopVideo()
    {
        VideoManager.Instance.StopVideo((int)GetSideLocation);
    }

    public virtual void ResetAnimation()
    {
        // IconManager.Instance.HideIconAnimation(HideIcon, _iconTransform, openTime / 3);
        TypeWriterEffect.Instance?.ClearString();
    }
}