﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using DG.Tweening;
public class DoorSide : BaseSide
{
    //[SerializeField, Range(0, 179.9f)] float openRotation;
    //[SerializeField] Transform LeftDoor, RightDoor;
    //[SerializeField] PlayVideos playVideos;

    override public void OnGaze()
    {
        base.OnGaze();
        OpenDoors();
    }

    override public void OutOfGaze()
    {
        base.OutOfGaze();
        CloseDoors();
    }

    //Open doors when plane is (close to) facing camera
    void OpenDoors()
    {
        if (!openAnimation.isPlaying)
        {
            openAnimation[openAnimation.clip.name].time = 0;
        }
        openAnimation[openAnimation.clip.name].speed = 1;
        openAnimation?.Play();
        //LeftDoor.DOLocalRotate(new Vector3(90, 0, openRotation), (openTime / 3) * 2, RotateMode.Fast);
        //RightDoor.DOLocalRotate(new Vector3(90, 0, -openRotation), (openTime / 3) * 2, RotateMode.Fast);
        //playVideos.SetupVideoPlane((int)sideLocation, Transform);
    }

    //Close doors when video is done playing or plane is no longer facing camera
    void CloseDoors()
    {
        if (!openAnimation.isPlaying)
        {
            openAnimation[openAnimation.clip.name].time = openAnimation[openAnimation?.clip.name].length;
        }
        openAnimation[openAnimation.clip.name].speed = -1;
        openAnimation?.Play();
        //LeftDoor.DOLocalRotate(new Vector3(90, 0, 0), (openTime / 3) * 2, RotateMode.Fast);
        //RightDoor.DOLocalRotate(new Vector3(90, 0, 0), (openTime / 3) * 2, RotateMode.Fast);
        //playVideos.StopVideo();
    }

    public override void ResetAnimation()
    {
        base.ResetAnimation();
        openAnimation[openAnimation.clip.name].time = 0;
        openAnimation[openAnimation.clip.name].speed = -1;
        openAnimation?.Play();
    }
}
