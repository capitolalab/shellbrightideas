﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoManager : MonoBehaviour
{
    [NamedArrayAttribute(new string[] { "Top Video", "Bottom Video", "Left Video", "Right Video", "Back Video", "Front Video" }), SerializeField]
    Transform[] _videoPlaneTransforms;

    private struct VideoPlane
    {
        public Transform transform;
        public bool isPlaying;
        public VideoPlayer videoPlayer;
    }

    private VideoPlane[] _videoPlanes;

    public static VideoManager Instance = null;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void OnDestroy()
    {
        foreach (VideoPlane videoPlane in _videoPlanes)
        {
            videoPlane.videoPlayer.prepareCompleted -= CheckVideosPrepared;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        _videoPlanes = new VideoPlane[_videoPlaneTransforms.Length];

        for (int i = 0; i < _videoPlaneTransforms.Length; i++)
        {
            _videoPlanes[i].transform = _videoPlaneTransforms[i];
            _videoPlanes[i].isPlaying = false;
            _videoPlanes[i].videoPlayer = _videoPlanes[i].transform.GetComponent<VideoPlayer>();
            _videoPlanes[i].videoPlayer.Prepare();
            _videoPlanes[i].videoPlayer.playbackSpeed = 0;
            _videoPlanes[i].videoPlayer.Play();
            _videoPlanes[i].videoPlayer.frame = 2;
        }

        foreach (VideoPlane videoPlane in _videoPlanes)
        {
            videoPlane.videoPlayer.prepareCompleted += CheckVideosPrepared;
        }
    }

    public void PlayVideo(int side)
    {
        if (side < _videoPlanes.Length)
        {
            // for (int i = 0; i < _videoPlaneTransforms.Length; i++)
            // {
            //     if (_videoPlanes[i].isPlaying)
            //     {
            //         _videoPlanes[i].transform.GetComponent<VideoPlayer>().Stop();
            //         _videoPlanes[side].transform.GetComponent<MeshRenderer>().enabled = false;
            //         _videoPlanes[i].isPlaying = false;
            //     }
            // }
            _videoPlanes[side].videoPlayer.playbackSpeed = 1;
            _videoPlanes[side].videoPlayer.Play();
            _videoPlanes[side].transform.GetComponent<MeshRenderer>().enabled = true;
            _videoPlanes[side].isPlaying = true;
        }
    }

    public void StopVideo(int side)
    {
        if (side < _videoPlanes.Length)
        {
            _videoPlanes[side].videoPlayer.playbackSpeed = 0;
            _videoPlanes[side].videoPlayer.Play();
            _videoPlanes[side].videoPlayer.frame = 2;
            _videoPlanes[side].transform.GetComponent<MeshRenderer>().enabled = false;
            _videoPlanes[side].isPlaying = false;
        }
    }

    public void StopPlayingAllVideos()
    {
        for (int i = 0; i < _videoPlanes.Length; i++)
        {
            _videoPlanes[i].videoPlayer.playbackSpeed = 0;
            _videoPlanes[i].videoPlayer.Play();
            _videoPlanes[i].videoPlayer.frame = 2;
            _videoPlanes[i].isPlaying = false;
        }
    }

    public void CheckVideosPrepared(VideoPlayer source)
    {
        foreach (VideoPlane videoPlane in _videoPlanes)
        {
            if (!videoPlane.videoPlayer.isPrepared)
                return;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            foreach (VideoPlane videoPlane in _videoPlanes)
            {
                videoPlane.videoPlayer.StepForward();
            }
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            foreach (VideoPlane videoPlane in _videoPlanes)
            {
                videoPlane.videoPlayer.playbackSpeed = 1;
            }
        }
        if (Input.GetKeyDown(KeyCode.V))
        {
            foreach (VideoPlane videoPlane in _videoPlanes)
            {
                videoPlane.videoPlayer.playbackSpeed = 0;
            }
        }
    }
}
